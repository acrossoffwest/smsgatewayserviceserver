var h = require("./helper.js");
var c = require("./config.js");
var PhoneController = {
	validCodes: [
		'7999',
		'7924',
		'7983',
		'7914',
		'7991',
		'7964',
		'7950'
	],
	validator: null,
	next: function(prevItem, items, callback){
		var res = items.indexOf(prevItem);
		var count = items.length - 1;
		h.log("Length: " + count);
		h.log("Current place: " + res);
		if(res == -1){
			return callback(0);
		}else{
			if(res == count){
				return callback(0);
			}else if((res + 1) <= count){
				return callback(res + 1);
			}else{
				return callback(0);
			}
		}
	}
};
PhoneController.validator = function (phone, callbackEnd){
	if(h.empty(phone) || phone.length != 11 || h.empty(PhoneController.validCodes)){
		return callbackEnd("Input data empty or not valid", null);
	}
	h.async.mapSeries(
		PhoneController.validCodes,
		function(item, callback){
			if(phone.indexOf(item) == 0){
				return callback("end", true);
			}else{
				return callback(null, null);
			}
		},
		function(error, results){
			if(!h.empty(error) && error == "end"){
				return callbackEnd(null, true);
			}else if(!h.empty(error)){
				return callbackEnd(error, null);
			}else{
				return callbackEnd(null, null);
			}
		}
	);
};
module.exports = PhoneController;