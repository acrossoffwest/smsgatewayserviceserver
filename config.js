c = {
	logFile: "log/main.log",
	events: {
		log: "log/events.log",

		test: "test",
		connection : "connection",
		disconnect: "disconnect",
		sms: {
			send: "SMS_SEND",
			error: "SMS_ERROR",
			answer: "SMS_ANSWER",
		}
	},
	error:{
		logFile: "log/error.log",
		http: {
			400: {
				en: "Bad request",
				ru: "Плохой, не верный запрос"
			},
			401: {
				en: "Unauthorized",
				ru: "Не авторизован"
			},
			402: {
				en: "Payment Required",
				ru: "Необходима оплата"
			},
			
			404: {
				en: "Page not found",
				ru: "Страница не найдена"
			},

			301: {
				en: "Moved Permanently",
				ru: "Перемещено навсегда"
			},

		},
		main: {
			empty: {
				en: "Object is null",
				ru: "Объект пуст"
			},
		},
	},
	server:{
		logFile: "log/server.log",
		domains: {
			main: {
				domain: "vm18366.hv8.ru",
				type: "http",
				port: "80",
			},
		},
		http: { 
			port: 7777,
			module: require("express"),
		},
		webSocket:{
			port: 9999,
			module_http: require("http"),
			module: require("socket.io"),
		},
	},
	db: {
		type: "mongodb",
		host: "localhost",
		port: "443",
		user: "root",
		password: "password",
		name: "dbn"
	},
	template:{
		name: "swig",
		module: require("swig"),
	}
};

module.exports = c;