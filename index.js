var h = require("./helper.js");
var c = require("./config.js");
var pc = require("./PhoneController.js");
var prevSocket = 0;
h.webio.events(function(socket){
	h.log("Connected: " + h.util.inspect(Object.keys(h.webio.io.sockets.connected)));
	h.log("Clients: " + h.util.inspect(Object.keys(h.webio.io.engine.clients)));
});
var m = function(prSocket, jSendObj, call){
	var r = h.webio.io.sockets.connected[prSocket].emit(c.events.sms.send, jSendObj);
	call(r);
};
h.webio.listen();
h.server.createEvent(
	"GET",
	c.events.sms.send,
	function(req, res){
		var q = req.query;
		if(h.empty(q.phone) || h.empty(q.message)){
			res.send("Sended test sms");	
		}else{
			if(!h.empty(q.multi) && q.multi == 1){
				var regex = ",";
				if(!h.empty(q.regex)){
					regex = q.regex;
				}
				var arrPhone = q.phone.split(regex);
				h.async.mapSeries(
					arrPhone,
					function iteratee(item,callback){
						pc.validator(item, function(err, res){
							if(res){ 
								var jSendObj = '{"phone": "' + item + '", "message": "' + q.message + '"}';
								pc.next(prevSocket, Object.keys(h.webio.io.sockets.connected), function(res_e){
									prevSocket = Object.keys(h.webio.io.sockets.connected)[res_e];
									return callback(null, {socket: prevSocket, data: jSendObj});
								});
								//h.webio.io.sockets.connected["/#" + Object.keys(h.webio.io.engine.clients)[].emit(c.events.sms.send, jSendObj);
								
							}else{
								callback(null, {error: "Phone nubmer not valid", phone: item});
							}
						});
					},
					function done(error, results){
						if(error){
							h.err(error);
							res.send("Error: " + h.util.inspect(error));
						}else{
							h.async.eachSeries(results, function(item, call){
								if(!h.empty(item.socket)){
									h.webio.io.sockets.connected[item.socket].emit(c.events.sms.send, item.data);
									h.log(h.util.inspect(item.data));
								}
								setTimeout(function(){
									call()
								}, 500);
							}, function(){
								res.send("SMS sent.");
							});
						}
					}
				);
			}else{
				pc.validator(q.phone, function(error,result){
					if(result){
						h.log('{"phone": "' + q.phone + '", "message": "' + q.message + '"}');
						var jSendObj = '{"phone": "' + q.phone + '", "message": "' + q.message + '"}';						
						var t = h.webio.io.sockets.connected["/#" + Object.keys(h.webio.io.engine.clients)[0]].emit(c.events.sms.send, jSendObj);
						res.send("Message sended");		
					}else{
						h.err("Phone nubmer not valid");
						res.send("Phone nubmer not valid");		
					}
				});
				
			}
		}		
		
	}
);
h.server.listen();

/*
h.log("Start.");
h.model.initUserModels(h.model.userSchemes, function(err, res){
	if(err){
		h.err(err);
	}else{
		h.model.inModel("messages", res, function(error, model){
			if(error){
				h.log(error);
				h.log("End.");
				h.kill();
			}else{
				var nRec = new model({message: "test"});
				nRec.save(function(errS, resS){
					if(err){
						h.err(errS);
						h.log("End.");
						h.kill();
					}else{
						h.log(resS);
						h.log("End.");
						h.kill();
					}
				});
				
			}
		});
		
	}
});

socket.on(
	"test",
	function(data){
		h.log(data);
		socket.emit("test_answer", "Хуйня");
	}
);

old

var validCodes = [
	'7999',
	'7924',
	'7983',
	'7914',
	'7991',
	'7964',
	'7950'
];

var validator = function (phone, phoneCodes, callbackEnd){
	if(h.empty(phone) || h.empty(phoneCodes)){
		return callbackEnd("Input data empty", null);
	}
	h.async.mapSeries(
		phoneCodes,
		function(item, callback){
			if(phone.indexOf(item) == 0){
				return callback("end", true);
			}else{
				return callback(null, null);
			}
		},
		function(error, results){
			if(!h.empty(error) && error == "end"){
				return callbackEnd(null, true);
			}else if(!h.empty(error)){
				return callbackEnd(error, null);
			}else{
				return callbackEnd(null, null);
			}
		}
	);
};

end old

*/

h.log("End.");