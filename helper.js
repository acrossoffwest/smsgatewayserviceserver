var fs = require("fs");
var swig = require("swig");
h = {
	config: require("./config.js"),
	datetime: function getDateTime() {					// return current time and date 

	    var date = new Date();

	    var hour = date.getHours();
	    hour = (hour < 10 ? "0" : "") + (hour + 5);

	    var min  = date.getMinutes();
	    min = (min < 10 ? "0" : "") + min;

	    var sec  = date.getSeconds();
	    sec = (sec < 10 ? "0" : "") + sec;

	    var year = date.getFullYear();

	    var month = date.getMonth() + 1;
	    month = (month < 10 ? "0" : "") + month;

	    var day  = date.getDate();
	    day = (day < 10 ? "0" : "") + day;

	    return "Date: " + year + ":" + month + ":" + day + " -> Time: " + hour + ":" + min + ":" + sec;

	},
	
	logd: function(inLine){								// styles output just log message in console
		console.log(
			"---------------------------------------\n"
			+ h.datetime() + "\n" + inLine +
			"\n---------------------------------------"
		);
	},
	err: null,
	log: null,
	writeLog: function (inLine, fileName, callback){	// write log in set file with name fileName
		fs.exists(
			fileName,
			function(exists){
				if(exists){
					fs.appendFile(
						fileName,
						h.datetime() + "\n" + inLine + "\n",
						function(error){
							if(error){
								callback(error, null);
							}else{
								callback(null, "OK");
							}
						}
					);
				}else{
					fs.writeFile(
						fileName,
						h.datetime() + "\n" + inLine + "\n",
						function(error){
							if(error){
								callback(error, null);
							}else{
								callback(null, "OK");
							}
						}
					);
				}
			}
		);
	},
	empty: null,						// return true if object empty else object not empty return false
	swig: {								// module for templating in data variables set of "data" variable 
		publicPath: null,
		compile: function(data, public, file, callback){
			var template = swig.compileFile(file);
			this.publicPath = public;
			callback(template(data));
		}	
	},
	server:{},							// http server
	kill: function(){			
		process.exit();					//ended application
	},
	util: require("util"),				// Util
	string: {},
	error:{
		404: "404",
		EMPTY: "EMPTY"
	},
	getPage: function(opt, callback){  // get web page with Request module
		var request = require("request");
		request(opt, function(error, response, body){
			callback(error, response, body);
		});
	},
	html: {							// Decode html chars to normal string
		decode: function(inLine){
			var Entity = require("html-entities").AllHtmlEntities;
			var ent = new Entity();
			return ent.decode(inLine);
		}
	},
	_: require("underscore"),
	end: null,						
	cheerio: require("cheerio"),  // JQuery
	async: require("async"),      // Async module
	express: null,  // Server
	webio: {                      // Socket io
		port: null,
		serverSocket: null,
		io: null,
		events: null,
		listen: null
	},
	mongoose: require("mongoose"), // Work with mongodb
	model:{},
	modules:{
		random: require("random-js")(),
	}, 
	fn: {
		rand: {
			int: null,
		},
	}
};
/*	Modules function init	*/

	h.fn.rand.int = function(min, max){
		return h.modules.random.integer(min, max);
	};

/*	End modules function init	*/

/*	Logs init	*/
h.err = function (inLine){								// styles output error message in console
		
		h.writeLog("\n" + inLine + "\n", h.config.error.logFile, function(){
			console.log(
				"----------ERROR----------\n"
				+ h.datetime() + "\n" + inLine +
				"\n------------------------------"
			);	
		});
};
h.log = function (inLine){								// output just log message in console
	h.writeLog("\n" + inLine + "\n", h.config.logFile, function(){
			console.log(
				"\n"
				+ h.datetime() + "\n" + inLine +
				"\n"
			);	
		});
};
/*	End logs init	*/
/*	Setting config data	*/

	// WebSockets
	h.webio.port = h.config.server.webSocket.port;
	h.express = h.config.server.http.module;
	h.webio.serverSocket = h.config.server.webSocket.module_http.createServer();
	// Http Server


/*	End setting config data	*/

/*		Socket IO		*/

h.webio.io = h.config.server.webSocket.module(h.webio.serverSocket);
h.webio.events = function(callback){
	h.webio.io.on(
		"connection",
		function(socket){
			h.log("Client connect");
			callback(socket);
			socket.on(
				"disconnect",
				function(data){
					h.log("Client disconnect");
				}
			);
		}
	);
};
h.webio.listen = function(){
	h.webio.serverSocket.listen(h.webio.port, function(){
		h.log("Start server");
	});
}
/*		End socket IO		*/
h.end = function( msg, callback){
	h.logd(msg);
	callback();
};
h.empty = function(obj){
	if(h._.isNull(obj) || h.util.inspect(obj) == "''" || typeof obj == 'undefined') //obj == null || obj == ''
		return true
	else 
		return  false;
};
h.server = {
	port: h.config.server.http.port,
	name: h.config.server.domains.main.domain,
	type: h.config.server.domains.main.type,
	run: h.express(),
	createEvent: null,
	listen: null
};
h.server.createEvent = function(method,name,callback){
	h.logd(
		  "Event info: \n"
		+ "Name: " + name + "\n"
		+ "Method type: " + method
	);
	switch(method){
		case "GET":
			h.server.run.get("/" + name + "/",
				function(req, res){
					var q = req.query;
					if(!h.empty(q.kill)){
						h.kill();
					}
					callback(req, res);	
				}
			);
			break;
		case "POST":
			h.server.run.post(
				"/" + name + "/",
				function(req, res){
					var q = req.query;
					if(!h.empty(h.util.inspect(q.kill))){
						h.kill();
					}
					callback(req, res);
				}
			);
			break;
		case "PUT":
			h.server.run.put(
				"/" + name + "/",
				function(req, res){
					var q = req.query;
					if(!h.empty(h.util.inspect(q.kill))){
						h.kill();
					}
					callback(req, res);
				}
			);
			break;
		case "DELETE":
			h.server.run.delete(
				"/" + name + "/",
				function(req, res){
					var q = req.query;
					if(!h.empty(h.util.inspect(q.kill))){
						h.kill();
					}
					callback(req, res);
				}
			);
			break;
	};
};
h.server.listen = function(){
	h.log(h.server.port);
	h.server.run.listen(h.server.port);
	h.logd(
		"Server start info:" 
		+ "\nPort: " + h.server.port + ";" 
		+ "\nName: " + h.server.name  + ";"
		+ "\nType: " + h.server.type + ";"
	);
	
};
h.string = {
	find: {
		email: function(inText){
			if(h.empty(inText)){
				return null;
			}else{
				return inText.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);			
			}
		}
	}
};
/*	Model variables	*/
h.model = {
	host: h.config.db.host,
	type: h.config.db.type,
	dbname: h.config.db.name,
	schema: h.mongoose.Schema,
	userSchemes: null,
	userModels: null,
	initUserModels: null,
	connect: null,
	crud: null,
	getModel: null
};
h.model.userSchemes = [
		{
			name: "messages",
			schema: new h.model.schema(
				{
					type: String,
					phone: String,
					message: String,
					status: String,
					contact: String,
					notes: String
				}
			)
		},
		{
			name: "contacts",
			schema: new h.model.schema(
				{
					name: String,
					phone: String,
					type: String,
					company: String,
					notes: String
				}
			)
		}
];
h.model.initUserModels = function(schemaArrays, callEnd){
	var i = 0;
	h.model.connect =  h.mongoose.createConnection(h.model.type + "://" + h.model.host + "/" + h.model.dbname);
	h.log("Model connection data: \n" + h.model.type + "://" + h.model.host + "/" + h.model.dbname);
	if(h.empty(schemaArrays) || !h._.isArray(schemaArrays)){
			h.log("Is Empty: " + h.empty(schemaArrays));
			h.log("Is Array: " + h._.isArray(schemaArrays));
		callEnd("Input data not valid.", null);
	}else{
		h.async.mapSeries(
			schemaArrays, 
			function(item, call){
				if(!h._.isObject(item)){
					return call("Is not object", null);
				}else{
					return call(null, {name: item.name, model: h.model.connect.model(item.name, item.schema)});
				}
			},function(err, results){
				if(err){
					return callEnd(err, null);
				}else{
					return callEnd(null, results);
				}
			}
		);
	}
};
h.model.initUserModels(h.model.userSchemes, function(err, res){
	if(err){
		h.err(err);
	}else{
		h.model.userModels = res;
	}
}); 
h.model.inModel = function(modelName, models, call){ // before run initial initUserModels
	if(h.empty(modelName)){
		return call("Model name empty", null);
	}else if(h.empty(models)){
		return call("Models empty", null);
	}else if(!h._.isArray(models)){
		return call("Models not array", null);
	}else{
		h.async.mapSeries(
			models,
			function(item, callback){
				if(item.name == modelName){
					return callback("end", item.model);
				}else{
					return callback(null, null);
				}
			},
			function(err, results){
				if(err = "end"){
					return call(null, results[0])
				}else if(err){
					return call(err, null);
				}else{
					return call(null, results[0]);
				}
			}
		);
	}
};
/*	End model variables	*/
module.exports = h;